import React, { Component } from "react";
import Countries from "./countries";
import Filter from "./filter";
import Loader from "./loader";
import Popup from "./popup";
import "../index.css";

class App extends Component {
  state = {
    loader: true,
    countriesInfo: [],
    popup: "",
    filterRegion: "All-region",
    search: ""
  };

  componentDidMount = () => {
    fetch("https://restcountries.com/v3.1/all")
      .then((data) => data.json())
      .then((countriesInfo) => {
        this.setState({
          countriesInfo,
          loader: false,
        });
      })
      .catch((err) => {
        alert("Error in Fetching ! ");
        console.error(err);
      });
  };

  render() {
    return (
      <div tabIndex={-1} onKeyUp={this.findEscapeClicked}>
        <div onClick={this.cursorClick}>
        <Filter onFilter={this.filterByRegion} onSearch={this.searchCountry}></Filter>
        <Popup popup={this.state.popup}></Popup>
        <Loader loader={this.state.loader}></Loader>
        <Countries
          countriesInfo={this.state.countriesInfo}
          onPopup={this.popupCountry}
          filterRegion={this.state.filterRegion}
          search={this.state.search}
        ></Countries>
        </div>
      </div>
    );
  }

 
  cursorClick=()=>{
    if(this.state.popup !== ""){
        this.removePopup();
    }
  }

  findEscapeClicked = (e) => {
    if (e.key === "Escape" && this.state.popup !== "") {
      this.removePopup();
    }
  };


  removePopup = () => {
    this.setState({ popup: "" });
  };

  searchCountry=(e)=>{
    console.log(e.target.value)
    this.setState({search:e.target.value})
  }

  filterByRegion = (e) => {
    let region = e.target.value;
    this.setState({filterRegion:region})
  };


  popupCountry = (countryName) => {
    let popup = this.state.countriesInfo.map((countryInfo, index) => {
      if (
        countryInfo.name.official === countryName &&
        countryInfo.name.official !== "Bouvet Island"
      ) {
        return (
          <div className="popup-div" key={index}>
            <div>
              <img
                src={`${countryInfo.flags.png}`}
                alt=""
                className="popup-country-flag"
              />
            </div>
            <div>
              <div className="popup-content">{countryInfo.name.official}</div>
              <div className="">
                <span className="country-span-detail">Population : </span>
                {countryInfo.population}
              </div>
              <div className="">
                <span className="country-span-detail">Native Name</span>
                {Object.values(countryInfo.name.nativeName)[0].official}
              </div>
              <div className="">
                <span className="country-span-detail">Region : </span>
                {countryInfo.region}
              </div>
              <div className="">
                <span className="country-span-detail">Sub Region : </span>
                {countryInfo.subregion}
              </div>
              <div className="">
                <span className="country-span-detail">Capital : </span>
                {countryInfo.capital}
              </div>
              <div className="">
                <span className="country-span-detail">Currencies : </span>
                {Object.values(countryInfo.currencies)[0].name}
              </div>
              <div className="">
                <span className="country-span-detail">Languages : </span>
                {Object.values(countryInfo.languages)}
              </div>
              <button className=" back-button" onClick={this.removePopup}>
                Back
              </button>
            </div>
          </div>
        );
      }
    });
    this.setState({ popup });
    return popup;
  };

}

export default App;
