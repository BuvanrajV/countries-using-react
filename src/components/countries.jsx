import React, { Component } from "react";

class Countries extends Component {
  render() {
    if (this.props.filterRegion === "All-region" && this.props.search==="") {
      return (
        <div className="container">
          {this.props.countriesInfo.map((countryInfo, index) => {
            if (countryInfo.name.official === "Bouvet Island") {
              countryInfo.capital = "Oslo";
            }
            return (
              <div
                className="country-div"
                key={index}
                onClick={() => {
                  this.props.onPopup(countryInfo.name.official);
                }}
              >
                <img
                  src={`${countryInfo.flags.png}`}
                  alt=""
                  className="country-flag"
                />
                <div className="country-name">{countryInfo.name.official}</div>
                <div className="country-population">
                  <span className="country-span-detail">Population</span> :{" "}
                  {countryInfo.population}
                </div>
                <div className="country-region">
                  <span className="country-span-detail">Region</span> :{" "}
                  {countryInfo.region}
                </div>
                <div className="country-capital">
                  <span className="country-span-detail">Capital</span> :{" "}
                  {countryInfo.capital}
                </div>
              </div>
            );
          })}
        </div>
      );
    } else if (this.props.search !== "") {
      return (
        <div className="container">
          {this.props.countriesInfo.map((countryInfo, index) => {
            if (countryInfo.name.official === "Bouvet Island") {
              countryInfo.capital = "Oslo";
            }
            if (countryInfo.name.official.toUpperCase().includes(this.props.search.toUpperCase())) {
              return (
                <div
                  className="country-div"
                  key={index}
                  onClick={() => {
                    this.props.onPopup(countryInfo.name.official);
                  }}
                >
                  <img
                    src={`${countryInfo.flags.png}`}
                    alt=""
                    className="country-flag"
                  />
                  <div className="country-name">
                    {countryInfo.name.official}
                  </div>
                  <div className="country-population">
                    <span className="country-span-detail">Population</span> :{" "}
                    {countryInfo.population}
                  </div>
                  <div className="country-region">
                    <span className="country-span-detail">Region</span> :{" "}
                    {countryInfo.region}
                  </div>
                  <div className="country-capital">
                    <span className="country-span-detail">Capital</span> :{" "}
                    {countryInfo.capital}
                  </div>
                </div>
              );
            }
          })}
        </div>
      );
    } else {
      return (
        <div className="container">
          {this.props.countriesInfo.map((countryInfo, index) => {
            if (countryInfo.region === this.props.filterRegion) {
              if (countryInfo.name.official === "Bouvet Island") {
                countryInfo.capital = "Oslo";
              }
              return (
                <div
                  className="country-div"
                  key={index}
                  onClick={() => {
                    this.props.onPopup(countryInfo.name.official);
                  }}
                >
                  <img
                    src={`${countryInfo.flags.png}`}
                    alt=""
                    className="country-flag"
                  />
                  <div className="country-name">
                    {countryInfo.name.official}
                  </div>
                  <div className="country-population">
                    <span className="country-span-detail">Population</span> :{" "}
                    {countryInfo.population}
                  </div>
                  <div className="country-region">
                    <span className="country-span-detail">Region</span> :{" "}
                    {countryInfo.region}
                  </div>
                  <div className="country-capital">
                    <span className="country-span-detail">Capital</span> :{" "}
                    {countryInfo.capital}
                  </div>
                </div>
              );
            }
          })}
        </div>
      );
    }
  }
}

export default Countries;
