import React, { Component } from "react";

class Filter extends Component {
  state = {};

  render() {
    return (
      <div className="filterContainer">
        <div className="search" >
            <input type="search" placeholder="Search country" className="inputSearch" onChange={this.props.onSearch}/>
        </div>
        <div className="section-select">
          <select className="select" onChange={this.props.onFilter}>
            <option value="All-region">All region</option>
            <option value="Africa">Africa</option>
            <option value="Americas">Americas</option>
            <option value="Asia">Asia</option>
            <option value="Europe">Europe</option>
            <option value="Oceania">Oceania</option>
          </select>
        </div>
      </div>
    );
  }
}

export default Filter;
