import React, { Component } from 'react';

class Popup extends Component {
    state = {  } 
    render() { 
        return (
            <div className="popup-container">{this.props.popup}</div>
        );
    }
}
 
export default Popup;